const path = require("path");

module.exports = {
	mode: process.env.NODE_ENV === "production" ? "production" : "development",
	entry: "./src/index.js",
	resolve: {
		extensions: [".js", ".jsx"],
	},
	module: {
		rules: [
			{
				test: /\.js|jsx$/,
				exclude: /node_modules/,
				use: { loader: "babel-loader" },
			},
		],
	},
	output: {
		filename: "index.js",
		path: path.resolve(__dirname, "dist"),
		library: {
			name: "dummy-project",
			type: "umd",
		},
		globalObject: "this",
	},
	externals: {
		react: {
			root: "React",
			commonjs2: "react",
			commonjs: "react",
			amd: "react",
		},
		"react-dom": {
			root: "ReactDOM",
			commonjs2: "react-dom",
			commonjs: "react-dom",
			amd: "react-dom",
		},
	},
};
