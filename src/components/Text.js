import React, { useEffect, useState } from "react";

export function Text({ text }) {
	const [flag, setFlag] = useState("green");
	useEffect(() => {
		setTimeout(() => {
			setFlag("red");
		}, 2000);
	}, []);
	return <h1>{`${flag}: ${text}`}</h1>;
}
